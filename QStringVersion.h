#ifndef QSTRINGVERSION_H
#define QSTRINGVERSION_H


#include <QList>

class QStringVersion {
public:
    QStringVersion();
    QStringVersion(const QString &text);
    QStringVersion(const QList<int> &parts);

    bool isValid() const {return m_parts.size() > 0;}
    const QString &text() const {return m_text;}
    const QList<int> &parts() const {return m_parts;}
    quint32 bword4() const;

    static int cmp(const QStringVersion &v1, const QStringVersion &v2);


    bool operator==(const QStringVersion &v) const {
        return m_text == v.m_text;
    }
    bool operator!=(const QStringVersion &v) const {
        return m_text != v.m_text;
    }

    bool operator>(const QStringVersion &v) const {return cmp(*this, v) > 0;}
    bool operator>=(const QStringVersion &v) const {return cmp(*this, v) >= 0;}
    bool operator<(const QStringVersion &v) const {return cmp(*this, v) < 0;}
    bool operator<=(const QStringVersion &v) const {return cmp(*this, v) <= 0;}
private:
    QString m_text;
    QList<int> m_parts;
};


#endif // QSTRINGVERSION_H

#include "QStringVersion.h"

QStringVersion::QStringVersion() {

}

QStringVersion::QStringVersion(const QString &text) :
    m_text{text} {

    const QStringList txt_parts {text.split('.')};
    for (const QString &part : txt_parts) {
        bool ok;
        int val = part.toInt(&ok);
        if (ok) {
            m_parts += val;
        }
    }
}

QStringVersion::QStringVersion(const QList<int> &parts) :
    m_parts{parts} {

    for (int part : parts) {
        if (!m_text.isEmpty())
            m_text += QChar('.');
        m_text += QString::number(part);
    }
}

quint32 QStringVersion::bword4() const {
    quint32 ret {0};
    for (int i = 0; i < 4; ++i) {
        quint32 byte = i < m_parts.size() ? m_parts.at(i) & 0xFF : 0;
        ret <<= 8;
        ret |= byte;
    }
    return ret;
}

int QStringVersion::cmp(const QStringVersion &v1, const QStringVersion &v2) {
    int ret {0};
    int parts_cnt = qMax<int>(v1.m_parts.size(), v2.m_parts.size());
    for (int i {0}; (i < parts_cnt) && (ret == 0); ++i) {
        int p1 = i < v1.m_parts.size() ? v1.m_parts.at(i) : 0;
        int p2 = i < v2.m_parts.size() ? v2.m_parts.at(i) : 0;
        ret = p1 - p2;
    }
    return ret;
}

